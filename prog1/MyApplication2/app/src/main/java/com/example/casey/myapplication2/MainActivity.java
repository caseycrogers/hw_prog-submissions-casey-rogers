package com.example.casey.myapplication2;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Map<String, Double> aMap = new LinkedHashMap<String, Double>();
    TextView outputAge;
    EditText inputAge;
    ImageView animalImage;
    ImageView deadImage;

    String inputAnimal;
    String outputAnimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            inputAge.setText(savedInstanceState.getString("inputAge"));
        }

        aMap.put("Human", 1.0);
        aMap.put("Bear", 2.0);
        aMap.put("Cat", 3.2);
        aMap.put("Chicken", 5.33);
        aMap.put("Cow", 3.64);
        aMap.put("Deer", 2.29);
        aMap.put("Dog", 3.64);
        aMap.put("Duck", 4.21);
        aMap.put("Elephant", 1.14);
        aMap.put("Fox", 5.71);
        aMap.put("Goat", 5.33);
        aMap.put("Groundhog", 5.71);
        aMap.put("Guinea pig", 10.0);
        aMap.put("Hamster", 20.0);
        aMap.put("Hippopotamus", 1.78);
        aMap.put("Horse", 2.0);
        aMap.put("Kangaroo", 8.89);
        aMap.put("Lion", 2.29);
        aMap.put("Manbearpig", .25);
        aMap.put("Monkey", 3.2);
        aMap.put("Mouse", 20.0);
        aMap.put("Parakeet", 4.44);
        aMap.put("Pig", 3.2);
        aMap.put("Pigeon", 7.27);
        aMap.put("Rabbit", 8.89);
        aMap.put("Rat", 26.67);
        aMap.put("Sheep", 5.33);
        aMap.put("Squirrel", 5.0);
        aMap.put("Wolf", 4.44);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = (Spinner) findViewById(R.id.animal_spinner);
        Spinner spinner2 = (Spinner) findViewById(R.id.animal_spinner2);
        spinner.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(this);

        inputAge = (EditText) findViewById(R.id.inputAge);
        outputAge = (TextView) findViewById(R.id.outputAge);
        animalImage = (ImageView) findViewById(R.id.animalImage);
        deadImage = (ImageView) findViewById(R.id.animalImage);

        inputAge.addTextChangedListener(ageWatcher);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("inputAge", inputAge.getText().toString());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);
        }
    }

    private void updateImage(String animal) {
        if (animal.matches("Guinea pig")) {
            animal = "guineapig";
        }
        Resources res = getResources();
        int resID = res.getIdentifier(animal.toLowerCase(),
                "drawable", getPackageName());
        if (resID == 0) {
            animalImage.setImageResource(R.drawable.nullimg);
        }
        animalImage.setImageResource(resID);
    }

    private void updateAge() {
        if (inputAge.getText().toString().matches("")) {
            outputAge.setText("");
        } else {
            double input = Double.parseDouble(inputAge.getText().toString());
            double output = input/aMap.get(inputAnimal);
            if (output >= 71.0) {
                Toast.makeText(getApplicationContext(), "Oh no, the " + outputAnimal +
                        " is dead!", Toast.LENGTH_LONG).show();
            }
            output = output * aMap.get(outputAnimal);
            outputAge.setText(String.format("%.2f", output));
        }
    }

    private final TextWatcher ageWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateAge();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void onItemSelected(AdapterView<?> parent, View v, int pos, long id){
        String animal = (String) parent.getItemAtPosition(pos);
        if (parent.getId() == R.id.animal_spinner) {
            inputAnimal = animal;
        } else if (parent.getId() == R.id.animal_spinner2) {
            outputAnimal = animal;
            updateImage(animal);
        }
        updateAge();
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Do Nothing
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
package com.example.casey.prog2_1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.StandardCharsets;

/* Listen for the phone, create the notification */
public class WatchListenerService extends WearableListenerService {
    private static final String TAG = "aaro";
    private static final String START_ACTIVITY = "/start_activity";

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        if( messageEvent.getPath().equalsIgnoreCase( START_ACTIVITY ) ) {
            String value = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            Intent intent = new Intent(this, eqNotif.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            
            String temp = "#";
            String[] placeMag = value.split(temp);
            intent.putExtra("place", placeMag[0]);
            intent.putExtra("magnitude", placeMag[1]);
            intent.putExtra("distance", placeMag[2]);

            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            long[] vibrationPattern = {0, 500, 50, 300};
            final int indexInPatternToRepeat = -1;
            vibrator.vibrate(vibrationPattern, indexInPatternToRepeat);

            startActivity(intent);
        } else {
            super.onMessageReceived( messageEvent );
        }

    }
}

package com.example.casey.prog2_1;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.wearable.activity.WearableActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

/* Display the notification on the watch */
public class eqNotif extends WearableActivity {
    private static final String TAG = "aaro";
    private GoogleApiClient mApiClient;
    private String codeword = "shake";

    public void screenTapped(View view) {
        finish();
        /*
        mApiClient.connect();
        new Thread( new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "eqNotif (screenTapped): Message is going to be sent");
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    Log.d(TAG, "eqNotif (screenTapped): Message is sent");
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), codeword, "test".getBytes() ).await();
                }
            }
        }).start();
        */
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.stopService(new Intent(this, WatchListenerService.class));
        mApiClient.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

                /* Initialize the googleAPIClient for message passing */
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d(TAG, "eqNotify (onConnected): connected");
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eq_notif);
        //first, let's extract that extra hour information we passed over
        Intent intent = getIntent();
        if (intent == null) {
            Log.d(TAG, "eqNotif: " + "intent is null!!");
        }
        String place = intent.getStringExtra("place");
        String magnitude = intent.getStringExtra("magnitude");
        String distance = intent.getStringExtra("distance");

        String[] placeArr = place.split(",");
        String city = placeArr[0];
        String region = placeArr[1].substring(1);

        if (place != null) {
            //Set the text to the earthquake city
            TextView cityView = (TextView) findViewById(R.id.city);
            cityView.setText(city + ",");

            //Set the text to the earthquake region
            TextView regionView = (TextView) findViewById(R.id.region);
            regionView.setText(region);
        }
        if (magnitude != null) {
            //Set the text to the magnitude
            TextView magnitudeView = (TextView) findViewById(R.id.magnitudeText);
            magnitudeView.setText(magnitude + " magnitude");
        }
        if (distance != null) {
            //Set the text to the distance
            TextView magnitudeView = (TextView) findViewById(R.id.distanceText);
            magnitudeView.setText(distance + " km");
        }


    }
}

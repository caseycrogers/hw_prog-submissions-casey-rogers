package com.example.casey.prog2_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    private static final String TAG = "aaro";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i = new Intent(this, WatchListenerService.class);
        startService(i);
        //Nothing happens here.
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        this.stopService(new Intent(this, WatchListenerService.class));
    }
}
/*package com.example.casey.prog2_1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MobileListenerService extends Service {
    public MobileListenerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
*/
package com.example.casey.prog2_1;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.StandardCharsets;

/* Listen for the phone, create the notification */
public class MobileListenerService extends WearableListenerService implements MessageApi.MessageListener {
    String codeWord = "shake";
    private static final String TAG = "aaro";

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        if( messageEvent.getPath().equalsIgnoreCase( codeWord ) ) {
            String value = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            /*Intent intent = new Intent(this, PictureActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intent);*/

            Log.d(TAG, "MobileLIstenerService (onMessagReceived): WOOH MESSAGE RECEIVED");
        }
    }
}

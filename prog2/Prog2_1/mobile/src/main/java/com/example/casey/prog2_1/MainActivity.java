package com.example.casey.prog2_1;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;

/* Just start the earthquake watching service (eqWatcherService) */
public class MainActivity extends Activity {
    public BroadcastReceiver receiver;
    public ArrayList<String> eqArrayList = new ArrayList<String>();
    public ArrayList<String> notifArrayList = new ArrayList<String>();
    public ArrayList<Double[]> locArrayList = new ArrayList<Double[]>();
    ListView listView;
    String TAG = "aaro";
    ArrayAdapter<String> adapter;
    private GoogleApiClient mApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* stupid apiclient */
        mApiClient = new GoogleApiClient.Builder( this )
                /* location stuff */
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        mApiClient.connect();

        Intent i = new Intent(this, eqWatcherService.class);
        startService(i);
        Intent intent = new Intent(this, MobileListenerService.class);
        startService(intent);

        /* Earthquake list stuff */
        listView = (ListView) findViewById(R.id.eqList);

        /* new adapter */
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, eqArrayList);
        listView.setAdapter(adapter);listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);
                String placeMag = notifArrayList.get(position);

                notifyEarthquake("/start_activity", placeMag, locArrayList.get(position)[0],
                        locArrayList.get(position)[1]);
            }
        });

        /* stupid broadcaster bullshit */
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String place = intent.getStringExtra("place");
                String mag = intent.getStringExtra("mag");
                String dist = intent.getStringExtra("dist");
                String notifString = intent.getStringExtra("notif");
                Double lat = Double.parseDouble(intent.getStringExtra("lat"));
                Double lon = Double.parseDouble(intent.getStringExtra("lon"));
                eqArrayList.add(place + ":   " + mag + " magnitude, " + dist + " kilometers");
                notifArrayList.add(notifString);
                locArrayList.add(new Double[]{lat, lon});

                Log.d(TAG, "MaintActivity (onRecieve): " + "worked");
                adapter.notifyDataSetChanged();
            }
        };


    }


    /* Send message to WatchListenerService */
    private void notifyEarthquake(final String path, final String placeMag, Double lat, Double lon) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, placeMag.getBytes() ).await();
                }
            }
        }).start();
        Intent intent = new Intent(this, MapActivity.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("lat", String.valueOf(lat));
        intent.putExtra("lon", String.valueOf(lon));
        String mg = placeMag.split("#")[1];
        intent.putExtra("mag", mg);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
        this.stopService(new Intent(this, eqWatcherService.class));
        this.stopService(new Intent(this, MobileListenerService.class));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(eqWatcherService.sendWord)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }
}

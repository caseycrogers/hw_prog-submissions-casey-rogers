package com.example.casey.prog2_1;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/* This class connects to the watch (WatcherListenerService)
   and tells the watch about earthquakes. */
public class eqWatcherService extends Service {
    static final public String sendWord = "sendeq";


    private static final int INTERVAL = 10000;
    private static final int SECOND = 1000;

    private String prevTime = null;
    private String TAG = "aaro";
    private String place;
    private String mag;
    private String dist;

    /* Location stuff */
    private Double mLat = 0.0;
    private Double mLon = 0.0;
    LocationRequest mLocationRequest = new LocationRequest();

    private GoogleApiClient mApiClient;

    /* Time API */
    private String mUrlString = "http://www.timeapi.org/pdt/now";
    private String mTimeResponse = "";

    /* USGS API */
    private String eqURL = "http://earthquake.usgs.gov/fdsnws/event/1/";
    private boolean firstEQ = true;

    private static final String START_ACTIVITY = "/start_activity";



    public void sendEQ(String place, String mag, String dist, String notif, Double lat, Double lon) {
        Intent intent = new Intent(sendWord);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("place", place);
        intent.putExtra("mag", mag);
        intent.putExtra("dist", dist);
        intent.putExtra("notif", notif);
        intent.putExtra("lat", String.valueOf(lat));
        intent.putExtra("lon", String.valueOf(lon));

        LocalBroadcastManager mBroadcastManager =
                LocalBroadcastManager.getInstance(this);
        mBroadcastManager.sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        /* More location stuff */
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        /* Initialize the googleAPIClient for message passing */
        mApiClient = new GoogleApiClient.Builder( this )
                /* location stuff */
                .addApi(LocationServices.API)
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Looper.prepare();
                                Log.d(TAG, "eqWatcherService (onConnected): prepared");
                                LocationListener locListener = new UselessLocationListener();
                                LocationServices.FusedLocationApi
                                        .requestLocationUpdates(mApiClient, mLocationRequest, locListener);
                                /* Successfully connected, now make API calls */
                            }
                        }).start();
                        createAndStartTimer();
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Kick off new work to do
        mApiClient.connect();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    /* Send message to WatchListenerService */
    private void notifyEarthquake(final String path, final String placeMag, Double lat, Double lon) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, placeMag.getBytes() ).await();
                }
            }
        }).start();
        Intent intent = new Intent(this, MapActivity.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("mag", String.valueOf(mag));
        intent.putExtra("lat", String.valueOf(lat));
        intent.putExtra("lon", String.valueOf(lon));
        startActivity(intent);
    }


    /* Periodically check USGS and call notify when there's an earthquake */
    private void createAndStartTimer() {
        Log.d(TAG, "eqWatcherService (countdown): Started Countdown");

        CountDownTimer timer = new CountDownTimer(INTERVAL, SECOND) {
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "eqWatcherService (countdown): " + Float.toString(millisUntilFinished / SECOND));
            }

            public void onFinish() {
                Log.d(TAG, "eqWatcherService (countdown): Countdown Finished");
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Looper.prepare();
                        URL url;
                        try {
                            url = new URL(mUrlString);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            return;
                        }

                        HttpURLConnection urlConnection = null;
                        try {
                            urlConnection = (HttpURLConnection) url.openConnection();
                            urlConnection.connect();
                            InputStream in = urlConnection.getInputStream();
                            Scanner scanner = new Scanner(in);
                            //Scanners scan line by line: if your response is longer than 1 line, you need a loop
                            mTimeResponse = scanner.nextLine(); //parses the GET request into a string
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (urlConnection != null) {
                                urlConnection.disconnect();
                            }
                        }
                        if (!mTimeResponse.equals("")) {
                            if (prevTime == null) {
                                long milliseconds = System.currentTimeMillis();
                                Date currentDate = new Date(milliseconds);
                                long hourEarly = (long) (milliseconds - 3.6e6);

                                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                prevTime = formatter.format(hourEarly);
                                mTimeResponse = formatter.format(currentDate);

                                Log.d(TAG, "eqWatcherService (time): " + prevTime);
                                Log.d(TAG, "eqWatcherService (time): " + mTimeResponse);


                            }
                            /* Setup USGS */
                            try {
                                url = new URL(eqURL + "query?format=geojson&starttime=" +
                                        prevTime + "&endtime=" + mTimeResponse);

                                Log.d(TAG, "eqWatcherService (USGSQuery): " +
                                        eqURL + "query?format=geojson&starttime=" +
                                        prevTime + "&endtime=" + mTimeResponse);
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                                return;
                            }
                                /* Query USGS */
                                HttpURLConnection USGSConnection = null;
                                try {
                                    urlConnection = (HttpURLConnection) url.openConnection();
                                    urlConnection.connect();
                                    InputStream in = urlConnection.getInputStream();
                                    Scanner scanner = new Scanner(in);
                                    String JSONString = "";
                                    while (scanner.hasNext()) {
                                        JSONString += scanner.nextLine();
                                    }
                                    try {

                                        /* Parse the JSON object */
                                        JSONObject EQs = new JSONObject(JSONString);
                                        JSONObject metaData = EQs.getJSONObject("metadata");
                                        int eqCount = metaData.getInt("count");
                                        Log.d(TAG, Integer.toString(eqCount));
                                        if (eqCount > 0) {
                                            JSONObject earthquake =
                                                    EQs.getJSONArray("features").getJSONObject(eqCount - 1);
                                            JSONObject earthquakeProperties = earthquake.getJSONObject("properties");
                                            JSONObject earthquakeLoc = earthquake.getJSONObject("geometry");
                                            Double eqLon = earthquakeLoc
                                                    .getJSONArray("coordinates").getDouble(0);
                                            Double eqLat = earthquakeLoc
                                                    .getJSONArray("coordinates").getDouble(1);

                                            place = earthquakeProperties.getString("place");
                                            int i = place.lastIndexOf("of ");
                                            place = place.substring(i + 3);
                                            mag = earthquakeProperties.getString("mag");

                                            /* Get the current location, convert to distance */
                                            Location mLastLocation = LocationServices
                                                    .FusedLocationApi.getLastLocation(mApiClient);
                                            mLat = mLastLocation.getLatitude();
                                            mLon = mLastLocation.getLongitude();
                                            Log.d(TAG, "eqWatcherService (lat : lon): " +
                                                    String.valueOf(mLat) + " : " + String.valueOf(mLon));

                                            DecimalFormat DF = new DecimalFormat("#,###");
                                            String dist = DF.format(getDistance(mLat, mLon, eqLat, eqLon));

                                            /* add to the list of earthquakes in main */

                                            if (!firstEQ) {
                                                sendEQ(place, mag, dist, place + "#" + mag + "#" + dist,
                                                        eqLat, eqLon);
                                                notifyEarthquake(START_ACTIVITY, place + "#" + mag + "#" + dist,
                                                        eqLat, eqLon);
                                            }
                                            firstEQ = false;
                                        }



                                    } catch (JSONException e) {
                                        Log.d(TAG, "OH SHIT JSON CRASHED");
                                        e.printStackTrace();
                                    }


                                    //Scanners scan line by line: if your response is longer than 1 line, you need a loop
                                    /* got through the scanner */
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } finally {
                                    if (urlConnection != null) {
                                        urlConnection.disconnect();
                                    }
                                }
                            prevTime = mTimeResponse;
                        }

                    }
                }).start();

                // Start the timer again
                createAndStartTimer();
            }
        };
        timer.start();
    }


    /* This function gets the distance between two GPS coordiantes */
    public static Double getDistance(Double lat1, Double lng1, Double lat2, Double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double dist = (earthRadius * c)/1000.0;

        return dist;
    }


    /* Useless junk */
    /* THIS CLASS DOES LITERALLY NOTHING BUT ANDROID MAKES ME MAKE IT ANYWAY */
    private final class UselessLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location locFromGps) {
        }
    }
}
